Splunk Add-on Builder version 2.2.0
Copyright (C) 2005-2016 Splunk Inc. All Rights Reserved.

For documentation, see: http://docs.splunk.com/Documentation/AddonBuilder/latest
