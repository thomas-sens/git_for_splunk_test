import cherrypy

import datetime
from aob.aob_common.builder_constant import *

def format_response_cookie():
    cookies = {}
    cherry_cookies = cherrypy.response.cookie
    cookie_names = cherry_cookies.keys()
    for k in COOKIE_KEYS:
        if k in cookie_names:
            cookies[k] = {'value': cherry_cookies[k].value,
                          'expires': cherry_cookies[k]['expires'],
                          'path': cherry_cookies[k]['path']}
    return cookies


def _generate_cookie_expires():
    d = datetime.datetime.utcnow() + datetime.timedelta(
        days=COOKIE_EXPIRES_DAY)
    return d.strftime('%a, %d %b %Y %H:%M:%S GMT')


def set_current_ta_project(ta_name):
    cherrypy.response.cookie[TA_NAME] = ta_name
    cherrypy.response.cookie[TA_NAME]['expires'] = _generate_cookie_expires()
    cherrypy.response.cookie[TA_NAME]['path'] = '/'


def get_current_ta_project():
    if TA_NAME in cherrypy.request.cookie:
        return cherrypy.request.cookie[TA_NAME].value
    else:
        return None


def delete_current_ta_project():
    cherrypy.response.cookie[TA_NAME] = ""
    cherrypy.response.cookie[TA_NAME]['expires'] = 0
    cherrypy.response.cookie[TA_NAME]['path'] = '/'


def set_current_ta_display_name(name):
    cherrypy.response.cookie[TA_DISPLAY_NAME] = name
    cherrypy.response.cookie[TA_DISPLAY_NAME]['expires'] = _generate_cookie_expires()
    cherrypy.response.cookie[TA_DISPLAY_NAME]['path'] = '/'


def get_current_ta_display_name():
    if TA_DISPLAY_NAME in cherrypy.request.cookie:
        return cherrypy.request.cookie[TA_DISPLAY_NAME].value
    else:
        return None


def delete_current_ta_display_name():
    cherrypy.response.cookie[TA_DISPLAY_NAME] = ""
    cherrypy.response.cookie[TA_DISPLAY_NAME]['expires'] = 0
    cherrypy.response.cookie[TA_DISPLAY_NAME]['path'] = '/'


def set_built_flag(built):
    cherrypy.response.cookie[BUILT_FLAG] = built
    cherrypy.response.cookie[BUILT_FLAG]['expires'] = _generate_cookie_expires(
    )
    cherrypy.response.cookie[BUILT_FLAG]['path'] = '/'


def get_built_flag():
    if BUILT_FLAG in cherrypy.request.cookie:
        return cherrypy.request.cookie[BUILT_FLAG].value
    else:
        return None


def delete_built_flag():
    cherrypy.response.cookie[BUILT_FLAG] = ""
    cherrypy.response.cookie[BUILT_FLAG]['expires'] = 0
    cherrypy.response.cookie[BUILT_FLAG]['path'] = '/'
