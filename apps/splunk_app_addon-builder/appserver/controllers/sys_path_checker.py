import sys
import os
bin_dir = os.path.join(os.environ['SPLUNK_HOME'], 'etc', 'apps',
                                'splunk_app_addon-builder', 'bin')
lib_dir = os.path.join(bin_dir, 'splunk_app_add_on_builder')

for d in [bin_dir, lib_dir]:
    if d not in sys.path:
        sys.path.insert(0, d)
