import _ from "lodash";
import defiant from "defiant";
// The replacer is a number so that even replace "null" in a string will not broke json structure
const replaceNull = function(data, replacer = "100") {
    return JSON.parse(JSON.stringify(data).replace(/null/g, replacer));
};

const _getHighlightLines = function(snapshot, xpath) {
    var hl = [];
    try {
        JSON.search(snapshot, xpath);
        var trace = JSON.trace, jl = trace.length;
        /* magic code hacked from defiant */
        for (var j = 0; j < jl; j++) {
            for (var k = 0; k < trace[j][1] + 1; k++) {
                hl.push(trace[j][0] + k - 1);
            }
        }
    } catch (e) {
        return [];
    }
    return hl;
};

/**
 *
 * @param data
 * @param xpath single path or a list of path
 * @returns {Array}
 */
const getHighlightLines = function(snapshot, xpath) {
    if (_.isString(xpath)) {
        return _getHighlightLines(snapshot, xpath);
    } else if (xpath instanceof Array) {
        let highlightLines = [];
        let len = xpath.length;
        for (let i = 0; i < len; i++) {
            highlightLines.push(_getHighlightLines(snapshot, xpath[i]));
        }
        return highlightLines;
    }
};

export { replaceNull, getHighlightLines };
